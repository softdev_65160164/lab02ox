/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package com.mycompany.lab02ox;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author ASUS
 */
public class testOX {
    
    public testOX() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void checkWin_X_Horizontal1_output_true(){
        char[][] GameBoard = {{'X','X','X'},{'4','5','6'},{'7','8','9'}};
        char currentPlayer = 'X';
        boolean result = OX.checkWin(GameBoard, currentPlayer);
        assertEquals(true, result);    
    }
    
    @Test
    public void checkWin_X_Horizontal2_output_true(){
        char[][] GameBoard = {{'1','2','3'},{'X','X','X'},{'7','8','9'}};
        char currentPlayer = 'X';
        boolean result = OX.checkWin(GameBoard, currentPlayer);
        assertEquals(true, result);      
    }
    
    @Test
    public void checkWin_X_Horizontal3_output_true(){
        char[][] GameBoard = {{'1','2','3'},{'4','5','6'},{'X','X','X'}};
        char currentPlayer = 'X';
        boolean result = OX.checkWin(GameBoard, currentPlayer);
        assertEquals(true, result);    
    }
    
    @Test
    public void checkWin_O_Horizontal1_output_true(){
        char[][] GameBoard = {{'O','O','O'},{'4','5','6'},{'7','8','9'}};
        char currentPlayer = 'O';
        boolean result = OX.checkWin(GameBoard, currentPlayer);
        assertEquals(true, result);  
    }
    
    @Test
    public void checkWin_O_Horizontal2_output_true(){
        char[][] GameBoard = {{'1','2','3'},{'O','O','O'},{'7','8','9'}};
        char currentPlayer = 'O';
        boolean result = OX.checkWin(GameBoard, currentPlayer);
        assertEquals(true, result);      
    }
     
    @Test
    public void checkWin_O_Horizontal3_output_true(){
        char[][] GameBoard = {{'1','2','3'},{'4','5','6'},{'O','O','O'}};
        char currentPlayer = 'O';
        boolean result = OX.checkWin(GameBoard, currentPlayer);
        assertEquals(true, result);    
    }
    
    @Test
    public void checkWin_X_vertical1_output_true(){
        char[][] GameBoard = {{'X','2','3'},{'X','5','6'},{'X','8','9'}};
        char currentPlayer = 'X';
        boolean result = OX.checkWin(GameBoard, currentPlayer);
        assertEquals(true, result);    
    }
     
    @Test
    public void checkWin_X_vertical2_output_true(){
        char[][] GameBoard = {{'1','X','3'},{'4','X','6'},{'7','X','9'}};
        char currentPlayer = 'X';
        boolean result = OX.checkWin(GameBoard, currentPlayer);
        assertEquals(true, result);    
    }
     
    @Test
    public void checkWin_X_vertical3_output_true(){
        char[][] GameBoard = {{'1','2','X'},{'4','5','X'},{'7','8','X'}};
        char currentPlayer = 'X';
        boolean result = OX.checkWin(GameBoard, currentPlayer);
        assertEquals(true, result);    
    }
    
    @Test
    public void checkWin_O_vertical1_output_true(){
        char[][] GameBoard = {{'O','2','3'},{'O','5','6'},{'O','8','9'}};
        char currentPlayer = 'O';
        boolean result = OX.checkWin(GameBoard, currentPlayer);
        assertEquals(true, result);    
    }
    
    @Test
    public void checkWin_O_vertical2_output_true(){
        char[][] GameBoard = {{'1','O','3'},{'4','O','6'},{'7','O','9'}};
        char currentPlayer = 'O';
        boolean result = OX.checkWin(GameBoard, currentPlayer);
        assertEquals(true, result);    
    }
    
    @Test
    public void checkWin_O_vertical3_output_true(){
        char[][] GameBoard = {{'1','2','O'},{'4','5','O'},{'7','8','O'}};
        char currentPlayer = 'O';
        boolean result = OX.checkWin(GameBoard, currentPlayer);
        assertEquals(true, result);    
    }
    
    @Test
    public void checkWin_X_cross1_output_true(){
        char[][] GameBoard = {{'1','2','X'},{'4','X','6'},{'X','8','9'}};
        char currentPlayer = 'X';
        boolean result = OX.checkWin(GameBoard, currentPlayer);
        assertEquals(true, result);    
    }
     
    @Test
    public void checkWin_X_cross2_output_true(){
        char[][] GameBoard = {{'1','2','X'},{'4','X','6'},{'X','8','9'}};
        char currentPlayer = 'X';
        boolean result = OX.checkWin(GameBoard, currentPlayer);
        assertEquals(true, result);    
    }
    
    @Test
    public void checkWin_O_cross1_output_true(){
        char[][] GameBoard = {{'O','2','3'},{'4','O','6'},{'7','8','O'}};
        char currentPlayer = 'O';
        boolean result = OX.checkWin(GameBoard, currentPlayer);
        assertEquals(true, result);    
    }
    
    @Test
    public void checkWin_O_cross2_output_true(){
        char[][] GameBoard = {{'1','2','O'},{'4','O','6'},{'O','8','9'}};
        char currentPlayer = 'O';
        boolean result = OX.checkWin(GameBoard, currentPlayer);
        assertEquals(true, result);    
    }
    
    @Test
     public void checkDraw_O_output_true(){
         char[][] GameBoard = {{'1','O','3'},{'4','O','O'},{'O','8','9'}};
         char currentPlayer = 'O';
         boolean result = OX.checkDraw(GameBoard, currentPlayer);
         assertEquals(true, result);    
     }
     
     @Test
     public void checkDraw_X_output_true(){
         char[][] GameBoard = {{'X','2','X'},{'X','5','6'},{'7','X','X'}};
         char currentPlayer = 'X';
         boolean result = OX.checkDraw(GameBoard, currentPlayer);
         assertEquals(true, result);    
     }
}
