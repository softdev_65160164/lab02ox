/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.lab02ox;

/**
 *
 * @author ASUS
 */
class OX {

    static boolean checkWin(char[][] GameBoard, char currentPlayer) {
        if(checkRow(GameBoard,currentPlayer)){
            return true;
        }else if(checkCol(GameBoard,currentPlayer)){
            return true;
        }else if(checkCross1(GameBoard,currentPlayer)){
            return true;
        }else if(checkCross2(GameBoard,currentPlayer)){
            return true;
        }
        return false;
    }
    
    static boolean checkDraw(char[][] GameBoard, char currentPlayer){
        if(isDraw(GameBoard)){
            return true;
        }
        return false;
    }
        
    private static boolean checkRow(char[][] GameBoard, char currentPlayer){
        for(int i = 0;i< 3;i++){
            if(checkRow(GameBoard,currentPlayer,i)){
                return true;
            }
        }
        return false;
    }
    private static boolean checkRow(char[][] GameBoard, char currentPlayer, int i) {
        return GameBoard[i][0] == (currentPlayer)&& GameBoard[i][1] == (currentPlayer) && GameBoard[i][2] == (currentPlayer);
        
    }
    private static boolean checkCol(char[][] GameBoard, char currentPlayer){
        for(int j = 0;j< 3;j++){
            if(checkCol(GameBoard,currentPlayer,j)){
                return true;
            }
        }
        return false;
    }

    private static boolean checkCol(char[][] GameBoard, char currentPlayer, int j) {
        return GameBoard[0][j] == (currentPlayer) && GameBoard[1][j] == (currentPlayer) && GameBoard[2][j] == (currentPlayer);
    }

    private static boolean checkCross1(char[][] GameBoard, char currentPlayer){
        for(int i = 0;i< 3;i++){
            if(checkCross1(GameBoard,currentPlayer,i)){
                return true;
            }
        }
        return false;
    }
    private static boolean checkCross1(char[][] GameBoard, char currentPlayer,int i) {
        return GameBoard[i][i] == (currentPlayer) && GameBoard[i][i] == (currentPlayer) && GameBoard[i][i] == (currentPlayer);
    }
    
    private static boolean checkCross2(char[][] GameBoard, char currentPlayer){
        for(int j = 0;j< 3;j++){
            if(checkCross2(GameBoard,currentPlayer,j)){
                return true;
            }
        }
        return false;
    }
    private static boolean checkCross2(char[][] GameBoard, char currentPlayer,int j) {
        return GameBoard[j][2-j] == (currentPlayer) && GameBoard[j][2-j] == (currentPlayer) && GameBoard[j][2-j] == (currentPlayer);
    }

    private static boolean isDraw(char[][] GameBoard) {
        for(int i = 0;i< 3;i++){
            for(int j = 0;j<3 ;j++){
                if (GameBoard[i][j] != 'X' && GameBoard[i][j] != 'O'){
                    return true;
                }
            }
        }
        return false;
    }
    
}
