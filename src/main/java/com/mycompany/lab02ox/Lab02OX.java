/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.lab02ox;

import java.util.Scanner;

/**
 *
 * @author ASUS
 */
public class Lab02OX {
    static char[][] table = {{'-','-','-'},
    {'-','-','-'},
    {'-','-','-'}};
    static char currentPlayer = 'O';
    static String pos;
    static int count = 0;
    
    
    public static void main(String[] args) {
        printWelcome();
        tablePos();
        
        while(true){
            printTable();
            printTurn();
            inputPosition();
            if(checkWin()){
                printTable();
                printWin();
                break;
            }
            if(checkDraw()){
                printTable();
                printDraw();
                break;
            }
            changeTurn();
        }
    }   

    private static void printWelcome() {
        System.out.println("Welcome to OX game !");  
    }
    private static void tablePos(){
        System.out.println("1"+"|"+"2"+"|"+"3");
        System.out.println("-----");
        System.out.println("4"+"|"+"5"+"|"+"6");
        System.out.println("-----");
        System.out.println("7"+"|"+"8"+"|"+"9");
        System.out.println(" ");
    }
    private static void printTable(){
        for(int i = 0; i<3; i++){
            for(int j = 0; j<3; j++){
                System.out.print(table[i][j]+ " ");
            }
            System.out.println();   
        }
    }

    private static void printTurn() {  
        System.out.println("Turn "+currentPlayer);
        
    }

    private static void inputPosition() {
        Scanner kb = new Scanner(System.in);
        System.out.print("Please input position(1-9) : ");
        pos = kb.next();
        switch(pos){
            case"1":
                table[0][0] = currentPlayer;
                break;
            case"2":
                table[0][1] = currentPlayer;
                break;
            case"3":
                table[0][2] = currentPlayer;
                break;
            case"4":
                table[1][0] = currentPlayer;
                break;
            case"5":
                table[1][1] = currentPlayer;
                break;
            case"6":
                table[1][2] = currentPlayer;
                break;
            case"7":
                table[2][0] = currentPlayer;
                break;
            case"8":
                table[2][1] = currentPlayer;
                break;
            case"9":
                table[2][2] = currentPlayer;
                break;
            default:
                System.out.println("ERROR");
        }
        count++;
    }

    private static void changeTurn() {
        if (currentPlayer == 'O') {
            currentPlayer = 'X';
        } else {
            currentPlayer = 'O';
        }

    }

    private static boolean checkWin() {
        if(checkRow()){
            return true;     
        }else if(checkCol()){
            return true; 
        }else if(checkCross1()){
            return true; 
        }else if(checkCross2()){
            return true; 
        }
        return false;
    }
    
    private static void printWin() {
        System.out.println("Congratulation! Player "+currentPlayer+" win!");
    }
    
    private static boolean checkRow() {
        if((table[0][0] == currentPlayer && table[0][1] == currentPlayer && table[0][2] == currentPlayer)||
           (table[1][0] == currentPlayer && table[1][1] == currentPlayer && table[1][2] == currentPlayer)||
           (table[2][0] == currentPlayer && table[2][1] == currentPlayer && table[2][2] == currentPlayer)){
            return true;
        }
        return false;
    }
    
    private static boolean checkCol() {
        if((table[0][0] == currentPlayer && table[1][0] == currentPlayer && table[2][0] == currentPlayer)||
           (table[0][1] == currentPlayer && table[1][1] == currentPlayer && table[2][1] == currentPlayer)||
           (table[0][2] == currentPlayer && table[1][2] == currentPlayer && table[2][2] == currentPlayer)){
            return true;
        }
        return false;
    }
    private static boolean checkCross1() {
        if((table[0][0] == currentPlayer && table[1][1] == currentPlayer && table[2][2] == currentPlayer)){
            return true;
        }
        return false;
    }
    private static boolean checkCross2() {
        if((table[0][2] == currentPlayer && table[1][1] == currentPlayer && table[2][0] == currentPlayer)){
            return true;
        }
        return false;
    }

    private static boolean checkDraw() {
        if(count == 9){
            return true;
        }
        return false;
    }
    
    private static void printDraw() {
        System.out.println("The game ended in the draw!!!");
    }
    
}
